import { Component, OnInit } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { NgForm, FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { EmployeesComponent } from '../employees/employees.component';
import { EmployeeService } from '../service/employee.service';
import 'rxjs/add/observable/of';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
})

export class EmployeeComponent implements OnInit {
  employeeForm: FormGroup;
  title: string = "Create";
  employeeId: number;
  errorMessage: any;
  cityList: Array<any> = [];

  constructor(private _fb: FormBuilder, private _avRoute: ActivatedRoute,
    private _employeeService: EmployeeService, private _router: Router) {
    if (this._avRoute.snapshot.params["id"]) {
      this.employeeId = this._avRoute.snapshot.params["id"];
    }

    this.employeeForm = this._fb.group({
      employeeId: 0,
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      gender: ['', [Validators.required]],
      position: ['', [Validators.required]],
      company: ['', [Validators.required]],
      city: ['', [Validators.required]]
    })
  }

  ngOnInit() {

    this._employeeService.getCityList().subscribe(
      data => this.cityList = data
    )

    if (this.employeeId > 0) {
      this.title = "Edit";
      this._employeeService.getEmployeeById(this.employeeId)
        .subscribe(resp => this.employeeForm.setValue(resp)
          , error => this.errorMessage = error);
    }

  }

  save() {

    if (!this.employeeForm.valid) {
      return;
    }

    if (this.title == "Create") {
      this._employeeService.saveEmployee(this.employeeForm.value)
        .subscribe((data) => {
          this._router.navigate(['/employees']);
        }, error => this.errorMessage = error)
    }
    else if (this.title == "Edit") {
      this._employeeService.updateEmployee(this.employeeForm.value)
        .subscribe((data) => {
          this._router.navigate(['/employees']);
        }, error => this.errorMessage = error)
    }
  }

  cancel() {
    this._router.navigate(['/employees']);
  }

  get firstName() { return this.employeeForm.get('firstName'); }
  get lastName() { return this.employeeForm.get('lastName'); }
  get gender() { return this.employeeForm.get('gender'); }
  get position() { return this.employeeForm.get('position'); }
  get company() { return this.employeeForm.get('company'); }
  get city() { return this.employeeForm.get('city'); }
}  
