export class Employee {
  employeeId: number;
  firstName: string;
  lastName: string;
  gender: string;
  position: string;
  company: string;
  city: string;
}  
