﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AngularCRUD.Domain;
using Microsoft.Extensions.Configuration;

namespace AngularCRUD.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly ApContext _context;

        public EmployeeController()
        {
            _context = new ApContext();
        }

        [HttpGet]
        [Route("api/Employee/Index")]
        public IEnumerable<Employee> Get()
        {
            return _context.Employee;
        }

        [HttpPost]
        [Route("api/Employee/Create")]
        public int Create([FromBody] Employee employee)
        {
            _context.Employee.Add(employee);
            _context.SaveChanges();
            return employee.EmployeeId;
        }

        [HttpGet]
        [Route("api/Employee/GetEmployee/{id}")]
        public Employee GetEmployee(int id)
        {
            return _context.Employee.SingleOrDefault(n => n.EmployeeId == id);
        }
        [HttpPut]
        [Route("api/Employee/Edit")]
        public int Edit([FromBody]Employee employee)
        {
            _context.Employee.Update(employee);
            _context.SaveChanges();
            return employee.EmployeeId;
        }

        [HttpDelete]
        [Route("api/Employee/Delete/{id}")]
        public int Delete(int id)
        {
            var employee = GetEmployee(id);
            _context.Employee.Remove(employee);
            _context.SaveChanges();
            return employee.EmployeeId;
        }

        [HttpGet]
        [Route("api/Employee/GetCities")]
        public IEnumerable<City> GetCities()
        {
            return _context.City;
        }
    }
}