﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AngularCRUD.Domain
{
    public class ApContext : DbContext
    {
        public DbSet<Employee> Employee { get; set; }
        public DbSet<City> City { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=DOTA;Initial Catalog=AngularCRUD2;Integrated Security=True");
        }
    }
}
